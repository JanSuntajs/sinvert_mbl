Simple demonstration code for shift-invert diagonalization of a disordered XXZ chain.
=====================================================================================

The code in this repository provides basic functionality for generating sparse Hamiltonians of the spin 1/2 XXZ chain
with nearest and second-nearest neighbours interactions and random spin disorder. The original code was written by David J. 
Luitz's group, only the second-nearest neighbour coupling terms were added by Jan �untajs. The original code implementing
only the nearest-neighbours functionality can be used by checking out the branch nearest_eighbours_only. The contents of 
the original readme file are given in the next few paragraphs. 

This code provides very basic functionality to generate the sparse Hamiltonian of the spin 1/2 XXZ chain in a distributed way. 
Since we are concerned with shift-invert diagonalization limited to small system sizes, the basis is fully enumerated in a 
symmetry sector with fixed total magnetization to make the code easier to read. We also provide a code to measure diagonal operators (local Sz).

The main program sinvert.cc uses these data structures to communicate with the PETSC/SLEPC libraries to calculate interior eigenpairs of the Hamiltonian and local observables measured in these states.


The code is licenced with GPL v3. If you need further documentations and installation instructions, **please refer to the accompanying paper**:

[1] Francesca Pietracaprina, Nicolas Mace, David J. Luitz and Fabien Alet,    
*"Shift-invert diagonalization of large many-body localizing spin chains"*,     
**[arXiv:1803.05395](https://arxiv.org/abs/1803.05395)**

In case you use this code for academic purposes, please include a reference to this paper [1] in relevant publications.


INSTALLATION INSTRUCTIONS, PETSC AND SLEPC CONFIGURATION GUIDELINES
=====================================================================================
In order to configure PETSC library, follow the instructions given in the above paper. Once the user has cloned the appropriate git repository and once 
the corresponding directory structure has been set up, configure and install the PETSC library. Below is an example bash script used for the task. The example
instructions provide a way for compiling the library using the MPI versions of Intel's compilers. Massively parallel solver MUMPS is linked against the Intel's 
MKL versions of BLAS, LAPACK and ScaLAPACK libraries. Openmp threading is enabled in addition to MPI parallelization. 

```bash
export PETCS_DIR=$PWD/
echo $PETSC_DIR
export PETSC_ARCH=linux_real_mumps_intel_openmp_optimised
# export PETSC_ARCH=linux_real_strumpack_intel
echo $MKLROOT
MKLPATH=$MKLROOT
echo $MKLPATH
echo $PETSC_ARCH



./configure --with-cxx=mpiicpc --with-cc=mpiicc --with-fc=mpiifort \
--with-debugging=0 CFLAGS="-O3" \
FFLAGS="-O3" CXXFLAGS='-O3' \
--with-blas-lapack-dir=$MKLROOT/lib/intel64 --with-openmp --download-hwloc --with-threadcomm --with-pthreadclasses \
--download-mumps=yes --download-metis=yes --download-parmetis=yes --download-spooles=yes --download-plapack=1 \
--with-shared-libraries=1 --with-scalapack-lib="-L$MKLROOT/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64" --with-scalapack-include=$MKLROOT/include


make PETSC_DIR=$HOME/petsc_dir/petsc PETSC_ARCH=linux_real_mumps_intel_openmp_optimised

make PETSC_DIR=$HOME/petsc_dir/petsc PETSC_ARCH=linux_real_mumps_intel_openmp_optimised check
```
In order to configure the SLEPC library, again follow the instructions in the above paper and first create the required folder structure. Once that has been done, 
issue the ```./configure``` command in the library's root folder and follow the onscreen instructions. The PETSC and SLEPC libraries are now installed. 

In order to compile the main program for generation and diagonalization of a XXZ Hamiltonian, first clone this repository to your local machine. Create a ```/build```
subdirectory in the project's root folder and move into the newly created directory. CMAKE needs to be installed on the system so that the appropriate makefiles for
the project can be generated automatically. In case the project is being installed on a linux machine, issue the following command(s): 
```
cmake -DMACHINE=linux ../src

make
```
This will first generate the necessary makefile(s) and then compile the program with all of its dependencies. Inspect the contents of the ```CMakeLists.txt``` file 
in the ```/src``` directory. If needed, comment/uncomment the relevant lines in the ```/src/conf/linux.cmake``` file. This creates a ```sinvert``` executable in the 
```/build``` folder.

RUNNING THE PROGRAM, SENDING JOBS TO THE CLUSTER
=====================================================================================
Since we are interested in obtaining the eigenvalues of large, sparse Hermitian matrices 
near the center of the spectrum, we use the shift-and-invert method based on LU decomposition approach. The code allows for simple changing of various model and diagonalization
parameters at runtime by means of a ```slepc.options``` configuration file saved in the executable's directory. An example of such a configuration file is given below:
```
-L 16
-nup 8
-J1 2.0
-J2 2.0
-V1 1.1
-V2 1.1
-W 1.0
-eps_type krylovschur
-eps_nev 20
-st_type sinvert
-st_ksp_type preonly
-st_pc_type lu
-st_pc_factor_mat_solver_type mumps
-mat_mumps_icntl_28 2
-mat_mumps_icntl_29 2
```
This would create a system of length 16 with 8 up spins while also setting the exchange interactions and disorder parameters to their corresponding values. 
Note that the last two options are important since they enable the parallel execution of the program.
In order to run the job on 
a cluster running SLURM, one could use the following example script: 
```bash
#!/bin/bash

#SBATCH --time=00:59:59
#SBATCH --job-name batch_test_new
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --distribution=cyclic:cyclic
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4000


cd .
hostname
pwd

time mpiexec ./sinvert

```