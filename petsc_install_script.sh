#!/bin/bash

export PETCS_DIR=$PWD/

export PETSC_ARCH=linux_real_mumps_intel

./configure --download-metis --download-mumps
--with-blaslapack-dir=$MKLROOT --with-cc=icc --with-cxx=icpc --with-fc=ifort
--with-threadsafety --with-log=0 --with-openmp
--with-scalapack-include=$MKLROOT/include --with-scalapack-dir=$MKLROOT/lib/intel64/libmkl_scalapack.a

make all

make PETSC_DIR=$HOME/petsc_dir/petsc PETSC_ARCH=linux_real_mumps_intel