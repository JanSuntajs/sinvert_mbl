/*  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Copyright 2018, David J. Luitz (C)

    DOCUMENTATION AND EXAMPLES:
    
    Binary I/O:
    https://www.mcs.anl.gov/petsc/petsc-current/src/vec/vec/examples/tutorials/ex5.c.html
*/
static char help[] = "Shift invert demonstration code for the random field XXZ chain. (C) 2018 David J. Luitz \n\n"
"The command line options are:\n"
"  -L = length of the chain\n"
"  -Delta <Delta>, where <Delta> = XXZ anisotropy. \n"
"  -J <J>, where <J> = XX coupling. \n"
"  -nup <nup>, where <nup> = nup sector (0, 1, 2 etc) defined by number of up spins. \n"
"  -W <W>, for intensity of field disorder. \n"
"  -random_seed <ds>, disorder seed. \n"
"\n";

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <fstream>
# include <random>
# include <slepceps.h>
# include <petscviewer.h>
# include "Basis.h"
# include "Hamiltonian.h"
# include "Operator.h"


// A declaration for a function that saves a vector to disk in binary format
PetscErrorCode MyVecDump(const char fname[],PetscBool skippheader,PetscBool usempiio,Vec x);


int main(int argc,char **argv)
{   


    // Parse the command-line arguments:
    for(int i=0; i<argc; i++)
    {

        std::cout << "argv["<<i<<"]: " << argv[i] << std::endl;
    }

    // Read the command-line arguments that provide the 
    // information needed for file storage
    std::string modpar, syspar, out_save, desc;
    std::string storage_dir; // where to store results
    std::string filename; // filename for storing data
    // make sure there are no errors if args are not present 

    // parsing the parameters 
    {
        if (argc>1)
        {
            
            modpar = argv[1];
        } else {
            
            modpar = "";
        }
        if (argc>2)
        {

            syspar = argv[2];
        } else {

            syspar = "";
        }
        if (argc>3) 
        {

            out_save = argv[3];
        } else {

            out_save = "";
        }
        if (argc>4)
        {

            desc = argv[4];
        } else {

            desc = "";
        }
    }
    // prepare folder structure
    storage_dir = out_save + "/" + syspar + "/" + modpar;
    // storage_dir = out_save;
    // mkdir(storage_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    std::system(("mkdir -vp " + storage_dir).c_str());
    std::cout << storage_dir << std::endl;

    
    // Petsc variables
    PetscErrorCode ierr;
    Mat            H; // PETSC sparse matrix  
    PetscInt       L; // length of the chain
    PetscInt       random_seed; // seed of the random number generator
    PetscInt       nup;  // up spin sector
    PetscReal      V1=1.1;  // Sz Sz coupling nearest neighbours
    PetscReal      V2=1.1; // Sz Sz coupling second nearest neighbours
    PetscReal      J1=2.0; // Sx Sx + Sy Sy coupling nearest neighbours
    PetscReal      J2=2.0; // Sx Sx + Sy Sy coupling second nearest neighbours
    PetscReal      W; //disorder strength
    PetscBool      output_matrix=PETSC_FALSE; // flag for writing matrix to Matrix Market format

    //// Set up MPI and SLEPC context
    //// Initializes the PETSc database and MPI.
    //// PetscInitialize() calls MPI_Init() if 
    //// that has yet to be called.
    SlepcInitialize(&argc,&argv,"slepc.options",help);
    int myrank, mpisize, random_seed_int;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpisize);

    // parse options
    PetscOptionsGetReal(NULL, NULL,"-V1",&V1,NULL);
    PetscOptionsGetReal(NULL, NULL,"-V2",&V2,NULL);
    PetscOptionsGetReal(NULL, NULL,"-J1",&J1,NULL);
    PetscOptionsGetReal(NULL, NULL,"-J2",&J2,NULL);
    PetscOptionsGetInt(NULL, NULL,"-L",&L,NULL);
    PetscOptionsGetInt(NULL, NULL,"-random_seed",&random_seed,NULL);
    PetscOptionsGetInt(NULL, NULL,"-nup",&nup,NULL);
    PetscOptionsGetReal(NULL, NULL,"-W",&W,NULL);
    PetscOptionsGetBool(NULL, NULL,"-output_matrix",&output_matrix,NULL);


    
    Basis basis(L,nup); // generate basis using nup conservation law
    {

        std::mt19937 gen(random_seed); //Standard mersenne_twister_engine seeded with rd()
        std::uniform_real_distribution<> disorder_dist(-W, W);
        std::vector<double> fields(L);
        for(size_t i=0;i<L; i++) fields[i] = disorder_dist(gen);


        // NOTE: 2*J1, 2*J2 is for compatibility with HCB code
        XXZHamiltonian hamiltonian(&basis, V1, V2, 2.*J1, 2.*J2, fields);

        std::vector<int> d_nnz, o_nnz;
        int nnz;

        

        // prepare for parallel distributed calculation of matrix elements
        auto start_end_pair=basis.get_mpi_ownership_range(myrank, mpisize);
        PetscInt Istart=start_end_pair.first;
        PetscInt Iend=start_end_pair.second;

        nnz = hamiltonian.calculate_nnz(Istart, Iend, d_nnz, o_nnz); // Preparation run, analyzing number of nonzero elements

        // the actual calculation of matrix elements
        std::vector<int> rows, cols;
        std::vector<double> entries;
        rows.reserve(nnz);
        cols.reserve(nnz);
        entries.reserve(nnz);
        hamiltonian.calculate_sparse_rows(Istart, Iend, rows, cols, entries);


        //// Creates a sparse parallel matrix in AIJ format
        //// (the default parallel PETSc format). For good 
        //// matrix assembly performance the user should 
        //// preallocate the matrix storage by setting the
        //// parameters d_nz (or d_nnz) and o_nz (or o_nnz). 
        //// By setting these parameters accurately, performance
        //// can be increased by more than a factor of 50.
        MatCreateAIJ( PETSC_COMM_WORLD, Iend-Istart, PETSC_DECIDE, basis.get_size(),
                     basis.get_size(), 0, d_nnz.data(), 0, o_nnz.data(), &H);

        //// Sets up the internal matrix data structures for later use.
        MatSetUp(H);

        //STORE VALUES
        if (myrank==0) 
        {
            std::cout << " Storing matrix... " << std::flush;
            std::cout << " Number of nonzero matrix elements is " << entries.size() << std::flush;
        }
        for(size_t i=0; i<entries.size(); i++)
        {
            MatSetValue(H,rows[i],cols[i],entries[i],INSERT_VALUES);
        }
        if (myrank==0) std::cout << "   done. " << std::endl;

    }

    if (myrank==0) std::cout << " Assembly... " << std::flush;
    MatAssemblyBegin(H,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(H,MAT_FINAL_ASSEMBLY);
    if (myrank==0) std::cout << " done. " << std::endl;

    if (output_matrix) {
      PetscViewer    view;
      PetscViewerASCIIOpen(PETSC_COMM_WORLD,"Matrix.mtx",&view);
      PetscViewerPushFormat(view,PETSC_VIEWER_ASCII_MATRIXMARKET);
      MatView(H,view);
      PetscViewerPopFormat(view);
      PetscViewerDestroy(&view);
    }


    ////////////////////////////////////
    //  Solve Eigenproblem
    ////////////////////////////////////
    {
        //// Set up the EPS framework
        //// Abstract SLEPc object that manages 
        //// all the eigenvalue problem solvers.

        EPS eps;
        PetscInt nconv;
        PetscScalar kr, ki;
        PetscScalar E0, E1, E;
        EPSCreate( PETSC_COMM_WORLD, &eps);
        EPSSetOperators( eps, H, NULL);
        EPSSetProblemType( eps, EPS_HEP ); // hermitian problem

        ////////////////////////////////////
        //  Determine the extremal
        //  eigenvalues.
        ////////////////////////////////////

        // Calculate maximal energy
        EPSSetWhichEigenpairs( eps, EPS_LARGEST_REAL);
        EPSSolve(eps);
        EPSGetConverged(eps, &nconv);
        EPSGetEigenvalue(eps,0, &kr, &ki);
        E1=PetscRealPart(kr);
        if(0==myrank){std::cout << "        E1 = " << E1 << std::endl;}

        // Calculate minimal energy
        EPSSetWhichEigenpairs( eps, EPS_SMALLEST_REAL );
        EPSSolve(eps);
        EPSGetConverged(eps, &nconv);
        EPSGetEigenvalue(eps,0, &kr, &ki);
        E0=PetscRealPart(kr);
        if(0==myrank){std::cout << "        E0 = " << E0 << std::endl;}
        EPSDestroy( &eps );


        ////////////////////////////////////
        //  Shift-and-invert part
        ////////////////////////////////////

        EPS eps_si;
        EPSCreate( PETSC_COMM_WORLD, &eps_si);
        EPSSetOperators( eps_si, H, NULL);
        EPSSetProblemType( eps_si, EPS_HEP );
        EPSSetFromOptions(eps_si);

        EPSSetWhichEigenpairs( eps_si, EPS_TARGET_REAL);
        EPSSetTarget(eps_si, (E0+E1)/2. );

        EPSSolve(eps_si);
        EPSGetConverged(eps_si, &nconv);

        Vec evecreal, tmp, eigvals;
        VecCreateSeq(PETSC_COMM_SELF,nconv, &eigvals);

        PetscViewer viewer;

        //// Get vector(s) compatible with the matrix, 
        //// i.e. with the same parallel layout.
        MatCreateVecs(H, &evecreal, NULL);
        MatCreateVecs(H, &tmp, NULL);

        // Set the vector for storing the eigenvalues

        //// uncomment if operator action analysis is desired
        // Operator op(&basis);

        std::cout.precision(15);
        if(0==myrank){
            std::cout << "-----------------------------------" << std::endl;
            std::cout << "Central eigenpairs:" << std::endl;
        }

	PetscBool get_wave_function=PETSC_FALSE; // change this to obtain the wave-function
        for(int i=0; i<nconv; i++)
        {
            EPSGetEigenpair(eps_si, i, &kr, &ki, evecreal, tmp); 
            VecSetValues(eigvals, 1, &i, &kr, INSERT_VALUES);
            //eigvals[i]=kr;
            //double Sz; 
            //size_t operator_site=3;
            //VecCopy(evecreal, tmp);
            //op.apply_Siz(operator_site,&tmp);
            //VecDot(evecreal,tmp, &Sz);

            //if(0==myrank) std::cout << "E("<<i<<") = " << kr << "   \t<psi|Sz["<<operator_site<<"]|psi> = " << Sz << std::endl;
            if(0==myrank) 
    	    {
        		std::cout << "E("<<i<<") = " << kr << std::endl;
    	    }

    	    if (get_wave_function)
	       {
        		Vec Vec_local;
        		if (mpisize==1) { VecCreateSeq(PETSC_COMM_SELF,basis.get_size(),&Vec_local); VecCopy(evecreal,Vec_local); } // if the problem is sequential
        		else // if the problem is parallelized using MPI
        		  { 
                    //// Creates an output vector and a scatter context used to copy all
                    //// vector values into the output vector on the zeroth processor  
        		    VecScatter ctx; VecScatterCreateToZero(evecreal,&ctx,&Vec_local);
                    //// Begins a generalized scatter from one vector to another. 
        		    VecScatterBegin(ctx,evecreal,Vec_local,INSERT_VALUES,SCATTER_FORWARD);
                    //// Complete the scattering phase with VecScatterEnd(). 
        		    VecScatterEnd(ctx,evecreal,Vec_local,INSERT_VALUES,SCATTER_FORWARD);
        		    VecScatterDestroy(&ctx);
        		  }

        		if (myrank==0)
        		  {
        		    PetscScalar * state; VecGetArray(Vec_local, &state);
        		    // now you can work on state --  e.g. access state[23];
        		    
        		    VecRestoreArray(Vec_local, &state);
        		  }
        		VecDestroy(&Vec_local);
            }


        }
        VecAssemblyBegin(eigvals);
        VecAssemblyEnd(eigvals);


        ////////////////////////////////////
        //  Store the results
        ////////////////////////////////////

        // Prepare filename for output
        filename = storage_dir + "/Part_eigvals_" + modpar + "_NS_" + std::to_string(random_seed) +".dat";
        // filename = storage_dir + "/Part_eigvals_" + modpar + "_NS_" + sprintf("%04d",random_seed) +".dat";
        // std::sprintf(filename, "%s/Part_eigvals_%s_NS_%04d.dat", storage_dir,modpar,random_seed);
        

        MyVecDump(filename.c_str(),PETSC_TRUE,PETSC_FALSE,eigvals);
        
        VecDestroy(&eigvals);
        VecDestroy(&evecreal);
        VecDestroy(&tmp);
        EPSDestroy( &eps_si);
	
    }

    /* Free work space */
    MatDestroy(&H);
    SlepcFinalize();
}


// Helper routines
PetscErrorCode MyVecDump(const char fname[],PetscBool skippheader,PetscBool usempiio,Vec x)
{   

    /*
    A wrapper routine for storing vectors to binary files.

    INPUT: 

    const char fname[] -> filename
    PetscBool skipheader - whether to skip binary file
                        header or not. If set to PETSC_FALSE,
                        the standard vector binary storage 
                        format is: 

                        int    VEC_FILE_CLASSID
                        int    number of rows
                        PetscScalar *values of all entries

                        If set to PETSC_TRUE, the first two
                        integer values are skipped.

    PetscBool usempiio -> whether MPI input output wiever is
                          used. Use PETSC_FALSE for now. 

    Vec x -> the vector to be stored to binary.  

    Note: to restore the file in numpy using numpy.fromfile()
    function when header lines were skipped, use: 

    dt = np.dtype('>f8')  # Big endianness
    np.fromfile(fname, dtype=dt)

    */
    MPI_Comm       comm;
    PetscViewer    viewer;
    PetscBool      ismpiio,isskip;

    PetscObjectGetComm((PetscObject)x,&comm);

    PetscViewerCreate(comm,&viewer);
    PetscViewerSetType(viewer,PETSCVIEWERBINARY);
    PetscViewerBinarySkipInfo(viewer);  // Do not generate the info file
    if (skippheader) { PetscViewerBinarySetSkipHeader(viewer,PETSC_TRUE); }
    PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);
    if (usempiio) { PetscViewerBinarySetUseMPIIO(viewer,PETSC_TRUE); }
    PetscViewerFileSetName(viewer,fname);

    VecView(x,viewer);

    PetscViewerBinaryGetUseMPIIO(viewer,&ismpiio);
    if (ismpiio) { PetscPrintf(comm,"*** PetscViewer[write] using MPI-IO ***\n"); }
    PetscViewerBinaryGetSkipHeader(viewer,&isskip);
    if (isskip) { PetscPrintf(comm,"*** PetscViewer[write] skipping header ***\n"); }

    PetscViewerDestroy(&viewer);
    return(0);
}






