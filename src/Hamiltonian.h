/*  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Copyright 2018, David J. Luitz (C)
*/

#ifndef HAMILTONIAN_H
#define HAMILTONIAN_H

#include <mpi.h>


/* Declare the XXZ hamiltonian class */
class XXZHamiltonian
{
    private:
        Basis * baseptr;
        size_t L;       // spin chain size 
        double V1;   // anisotropy parameter for nearest-neighbour interaction 
        double V2;   // anisotropy parameter for next-nearest-neighbour interaction 
        double J1;      // nearest-neighbour exchange
        double J2;      // next-nearest-neighbour exchange
        //list of J vals
        std::vector<double> Jlist{J1, J2};
        //list of delta vals
        std::vector<double> Vlist{V1, V2};
        std::vector<double> fields;
        int myrank, mpisize;

    public:
        XXZHamiltonian(Basis * baseptr__, double V1__,
                       double V2__, double J1__,
                       double J2__, std::vector<double> fields__);

        void calculate_sparse_rows(size_t begin_row_, size_t end_row_,std::vector<int> & row_idxs_,
                                   std::vector<int> & col_idxs_, std::vector<double> & entries_); 

        int calculate_nnz(size_t begin_row_, size_t end_row_, 
                          std::vector<int> & d_nnz_,
                          std::vector<int> & o_nnz_); 
};



XXZHamiltonian::XXZHamiltonian(Basis * baseptr__, double V1__,
                       double V2__, double J1__,
                       double J2__, std::vector<double> fields__):
    
    /* Member initialization list */
    baseptr(baseptr__),
    L(baseptr->get_L()),
    V1(V1__),
    V2(V2__),
    J1(J1__),
    J2(J2__),
    fields(fields__)

{

    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpisize);
    if(0==myrank)
    {
        std::cout << "[Hamiltonian] created with V1 = " << V1 << "." << std::endl;
        std::cout << "[Hamiltonian]              V2 = " << V2 << "." << std::endl;
        std::cout << "[Hamiltonian]              J1 = " << J1 << "." << std::endl;
        std::cout << "[Hamiltonian]              J2 = " << J2 << "." << std::endl;
        std::cout << "[Hamiltonian]              L = " << L << "." << std::endl;
        std::cout << "[Hamiltonian]              fields = ";
        for(size_t i=0; i<L; i++) std::cout << fields[i] << " ";
        std::cout << std::endl;
    }
}



int XXZHamiltonian::calculate_nnz(size_t begin_row_, size_t end_row_, std::vector<int> & d_nnz_, std::vector<int> & o_nnz_)
{   

    /*
    Code for determining the block-diagonal structure
    of the hamiltonian.  
    
    INPUT: 
    size_t begin_row_ - starting row index 
    size_t end_row_   - ending row index 
    // the hamiltonian matrix is divided into rows so that the hamiltonian 
    // can be constructed in parallel using mpi

    std::vector<int> & d_nnz_ - pointer to a vector that on output
                                contains the number of matrix 
                                elements in the diagonal block. Its size
                                equals end_row_ - begin_row_

    std::vector<int> & o_nzz_ - pointer to a vector that on output 
                                contains the number of matrix 
                                elements in an offdiagonal block. Its
                                size equals end_row_ - begin_row_


    RETURN: 
    
    int nzz - the number of nonzero matrix elements in a given subspace. 
    
    */

    d_nnz_.assign(end_row_-begin_row_,0);
    o_nnz_.assign(end_row_-begin_row_,0);
    int nnz = 0;

    size_t row_ctr=0;
    for(size_t r=begin_row_; r<end_row_; r++) // iterates over states
    {
        State state(baseptr->get_state(r)); // constructs a given state

        // offdiag part
        for(int y=1; y<=2; y++) // nearest and second-nearest neighbours 
        {   
            if (Jlist[y-1]!=0)
            {
                for(size_t x=0; x<L; x++) 
                {
                    if(state[x] != state[(x+y)%L]) // whether a spin-flip is possible   
                    {  
                        State newstate(state);
                        newstate.flip(x);
                        newstate.flip((x+y)%L);

                        size_t c = baseptr->get_index(newstate);

                        if((c>=end_row_) or (c<begin_row_))
                            o_nnz_[row_ctr]++;
                        else
                            d_nnz_[row_ctr]++;
                        nnz++;
                    }
                    
                }
            }
        }
        // diag part - a conter is increased by default
        d_nnz_[row_ctr]++; // count nonzeros
        nnz++;
        ++row_ctr;
    }
    return nnz;
}



void XXZHamiltonian::calculate_sparse_rows(size_t begin_row_, size_t end_row_,std::vector<int> & row_idxs_, std::vector<int> & col_idxs_, std::vector<double> & entries_)
{      

    // //list of J vals
    // std::vector<double> Jlist{J1, J2};
    // //list of delta vals
    // std::vector<double> Vlist{V1, V2};
    size_t row_ctr=0;
    for(size_t r=begin_row_; r<end_row_; r++)
    {
        State state(baseptr->get_state(r));

        // site part - random fields
        double diag=0.0;
        for(size_t x=0; x<L; x++)
        {
            if (state[x]) {diag-=0.5*fields[x];}
            else { diag+=0.5*fields[x];}
        }

        // offdiag part
        for(int y=1; y<=2; y++)
        {
            for(size_t x=0; x<L; x++)
            {

                if(state[x] != state[(x+y)%L]) // neighboring spins not equal
                {
                    diag-=0.25*Vlist[y-1];
                    if (Jlist[y-1]!=0.0)
                    {   
                        State newstate(state);
                        newstate.flip(x);
                        newstate.flip((x+y)%L);

                        size_t c = baseptr->get_index(newstate);

                        double offdiag=0.5*Jlist[y-1];

                        row_idxs_.push_back(r);
                        col_idxs_.push_back(c);
                        entries_.push_back(offdiag);
                    }
                }
                else
                {
                    diag+=0.25*Vlist[y-1];
                }
            }
        }
        row_idxs_.push_back(r);
        col_idxs_.push_back(r);
        entries_.push_back(diag);

        ++row_ctr;
    }
}




#endif
