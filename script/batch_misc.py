# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is a simple job submission script for
submitting jobs to the cluster runing SLURM
manager.

Contents of this module:

class Job() ->

    instantiates a Job() which contains all
    the necessary data for running a single
    job on a cluster. The class also
    implements methods for creating the
    required folder structure and parameter
    parsing routines.


class BatchSender() ->

    This class is used for preparing multiple
    jobs for queue submission. When a user
    provides a dictionary of different runtime
    parameters, all possible combinations of
    those are considered and a list of jobs
    for different combinations of parameters
    is created. Methods implemented in this
    class take care that the jobs are submitted
    to the SLURM running cluster.


class SubmittedScript() ->

    A class that stores a sbatch job submission
    script appropriate for our use case where
    realizations over different values of disorder
    are needed in the calculations. The script
    allows for both OpenMP and MPI types of
    parallelization.


"""


import os
import subprocess as sp
import argparse
import itertools as it
from collections import defaultdict
import shutil
import time

"""
Create the necessary folder structure.
"""

storage = ""


def mkdir_p(dir):
    """
    Make a directory if it does not
    exist yet.
    """
    if not os.path.exists(dir):
        os.mkdir(dir)


"""
Routines used for compatibility between the results of the
full and partial diagonalization code. This is particulary
relevant for data analysis, where the current code is
designed to look for certain filename patterns.
"""


class Job(object):

    """
    Class that stores a given job to be run on a cluster

    """
    pars = dict()  # all parameters one could supply to the job
    syspar = dict()  # system-related parameters (size, number of up spins ...)
    modpar = dict()  # module parameters - coupling constants etc.
    redefine = dict()  # alternative definition of parameters

    script = ""  # initializer of the slepc.options script
    system_parameters = ""  # string for system parameters
    module_parameters = ""  # string for module parameters
    working_directory = "."  # working directory of the sent program
    description = ""  # human readable description of the job

    # methods
    def __init__(self, pars_in, script_in, description_in, redefine_in={},
                 redefine_keys_in={}):
        super(Job, self).__init__()

        self.redefine = redefine_in
        self.script = script_in
        self.pars = pars_in.copy()
        self.rekey(redefine_keys_in)

        self.parse_pars()
        self.generate_string_templates()
        self.make_script()
        self.set_directory(description_in)

    def parse_pars(self, factor=2.):
        """
        Parse the provided parameters into correct
        form
        """

        formatting_Dict = {
            'NU': lambda x, y: {'NU': '{}'.format(x)},  # number of up spins
            'ne': lambda x, y: {'ne': '{}'.format(x)},  # number of holes
            's_size': lambda x, y: {'s_size': '{}'.format(x)},  # system size
            'J1': lambda x, y: {'J1': '{:+.5f}'.format(x)},  # n.n. exchange
            'J2': lambda x, y: {'J2': '{:+.5f}'.format(x)},  # s.n.n. exc.
            'V1': lambda x, y: {'V1': '{:+.5f}'.format(x)},  # n.n. interaction
            'V2': lambda x, y: {'V2': '{:+.5f}'.format(x)},  # s.n.n. int.
            'W': lambda x, y: {'W': '{:+.5f}'.format(x)},  # disorder parameter
            'base': lambda x, y: {'base': '{}'.format(x)},  # base present/not
            'MIN_SEED': lambda x, y: {'MIN_SEED': '{}'.format(x)},  # iteration
            'MAX_SEED': lambda x, y: {'MAX_SEED': '{}'.format(x)},  # iteration
            'nev': lambda x, y: {'nev': '{}'.format(x)},  # number of eigvals
            'dim': lambda x, y: {'dim': '{}'.format(x)},
        }

        formatting_Dict.update(self.redefine)
        pairs = zip(self.pars.iterkeys(), self.pars.itervalues())
        parsed_pars = dict()

        for (key, value) in pairs:

            parsed_pars.update(formatting_Dict[key](value, self.pars))

        self.pars = parsed_pars

    def generate_string_templates(self):
        """
        Generate a string describing
        both the system and module parameters.



        OUTPUT:

        system_parameters - a string, for example:

                                         D1SS0012_Ne006F_NU006

        """
        # s_size - integer, system size
        # dim - integer, system dimensionality
        # base - T, F whether a system has a base or not
        # ne - integer, number of holes in the system
        # nu - integer, number of up spins in the chain
        # NOTE: we reverse the roles of NU and ne for
        # compatibility with the hcb code
        sys_par_template = 'D{}SS{:0>4}_Ne{:0>3}{}_NU{:0>3}'

        self.system_parameters = sys_par_template.format(self.pars['dim'],
                                                         self.pars['s_size'],
                                                         self.pars['NU'],
                                                         self.base(),
                                                         self.pars['ne'])

        # different parts of Hamiltonian are separated by the _Mod string.
        # In this case, first part refers to the second-nearest neighbours
        # term, the second terms in the random disorder contribution,
        # the third term is the nearest neighbour contribution to both
        # exhange and interaction.
        mod_par_template = ('Mod_{}d0_ih_2_{}d0_dg_2_Mod_{}'
                            'd0_dg_4_Mod_{}d0_ih_2_{}'
                            'd0_dg_1_tof_0000')

        self.module_parameters = mod_par_template.format(self.pars['J2'],
                                                         self.pars['V2'],
                                                         self.pars['W'],
                                                         self.pars['J1'],
                                                         self.pars['V1'],
                                                         self.pars['W'])

    def make_script(self):
        """
        Generate the contents of a
        slepc.options file from
        which the runtime parameters are
        determined. The parameters are:

        -L: system size
        -nup: number of up spins
        -J1, J2, V1, V2: exchange and interaction
                         constants
        -W: disorder parameter
        -nev: number of eigenvalues to look for

        OTHER OPTIONS (not yet adjustable):

        -eps_type -> method to use when solving
                     the eigensystem
        -st_type ->  which spectral transformation
                     to use (shift and invert)
        -st_ksp_type, -st_pc_type -> type of
                     preconditioner to use
        -st_pc_type -> preconditioner type,
                     we use LU decomposition
        -st_pc_factor_mat_solver_type -> which
                     solver to use. We use MUMPS
        -mat_mumps_icntl_28 2
        -mat_mumps_icntl_29 2 -> these two parameters
                     tell mumps to use parallel methods

        """
        self.script = """
        -L {0}
        -nup {1}
        -J1 {2}
        -J2 {3}
        -V1 {4}
        -V2 {5}
        -W  {6}
        -eps_type krylovschur
        -eps_nev {7}
        -st_type sinvert
        -st_ksp_type preonly
        -st_pc_type lu
        -st_pc_factor_mat_solver_type mumps
        -mat_mumps_icntl_28 2
        -mat_mumps_icntl_29 2
        """.format(self.pars['s_size'], self.pars['NU'],
                   self.pars['J1'], self.pars['J2'],
                   self.pars['V1'], self.pars['V2'],
                   self.pars['W'], self.pars['nev'])

    def set_directory(self, description_in):
        """
        Set the working directory path -> the scripts and
        slepc.options files are copied to another location
        where they are ran from.

        The structure is as follows:

        - storage -> (global) user provided string for where
                     the files are to be stored
        - self.system_parameters -> top folder depends on the
                                    system details
        - self.module_parameters -> bottom folder depends on
                                    the parameter values
        """

        self.working_directory = storage + '/' + \
            description_in + '/' + 'Run' + '/' + \
            self.system_parameters + '/' + \
            self.module_parameters

    def rekey(self, rekey_dictionary):
        """
        Rename the specified parameters
        """

        for old, new in rekey_dictionary.items():
            self.parameters[new] = self.parameters.pop(old)

    def base(self):
        """
        Return the correct format for the presence of unit cells with base
        """
        base = self.pars['base']
        if int(base) > 1:
            return 'T'
        else:
            return 'F'


class BatchSender(object):
    """
    Prepare multiple jobs for queue submission.

    INPUT:

    description_in - string, a human readable job description
    redefine_in - a dictionary of parameter redefinitions
    redefine_keys_in - redefine keys in the parameter dictionary
    **args - dictionary - parameters over which we want to iterate

    Using the tools from the itertools library, we permute across
    all possible values of parameters and create a Job() object
    for each parameter combination. A corresponding job is then run
    on the cluster.

    """
    # script =

    parameters = dict()
    jobs = []
    memory = 4
    time = "23:59:59"
    program = "build/main"
    nodes = 1
    ntasks = 1
    threads = 1
    delay = 3
    optional = ""
    redefine = {}
    description = ""
    # Default paths
    log_fold = "Log"
    log_path = "Log.dat"
    out_save_path = "."  # root folder for saving output files

    # Default system parameter values
    default_Dict = {'NU': 0,
                    'ne': 0,
                    's_size': 10,
                    'J1': 0.0,
                    'J2': 0.0,
                    'V1': 0.0,
                    'V2': 0.0,
                    'W': 0.0,
                    'base': 0,
                    'MIN_SEED': 1,
                    'MAX_SEED': 1,
                    'nev': 10,
                    'dim': 1,
                    }

    file_names = defaultdict(lambda: ["main.cc", "build/main"],
                             {"partial_diag": [
                                 "src/sinvert.cc", "build/sinvert"]}
                             )

    def __init__(self, description_in, redefine_in={},
                 redefine_keys_in={}, **args):
        super(BatchSender, self).__init__()

        self.description = description_in  # a human readable job description
        self.redefine = redefine_in  # a dict of possible parameter redefs.
        self.redefine_keys = redefine_keys_in   # redef. keys in args dict
        self.queue_description = description_in.replace(" ", "_")
        self.arg_parser()  # take care of the provided command-line arguments
        vals = args.values()  # input pars values
        keys = args.keys()      # input pars keys
        parameters = self.default_Dict.copy()  # placeholder for job params

        # create a list of Job objects for each given parameter specification
        for i in it.product(*vals):
            add_items = dict(zip(keys, i))
            parameters.update(add_items)

            new_Job = Job(parameters, '', self.description,
                          self.redefine, self.redefine_keys)
            self.jobs.append(new_Job)

    def arg_parser(self):
        """
        Parse possible modes for job - compile, debug or norm.
        """
        parser = argparse.ArgumentParser(prog="Run multiple jobs.")
        parser.add_argument('mode', help="Mode for job - {}.".format(
            ", ".join(self.file_names.keys())), nargs='*', metavar='mode',
            default=['norm'])
        args = parser.parse_args()
        self.runtype = args.mode

    def run_jobs(self, no_queue=False, optional_in='',
                 optional_flag_string=""):
        """
        Sends a job to batch queue to execute
        the program. If no_queue = True, the
        program is run directly on the head
        node.

        """

        cwd = os.getcwd()  # this script's directory

        os.chdir('./..')

        for job in self.jobs:

            # find correct program name to execute
            self.program = parse_from_dict(self.runtype, self.file_names)[0][1]

            self.create_directory(job)
            self.set_output_log(job)
            self.set_arguments(job, optional_in)
            cd = job.working_directory
            if no_queue:

                os.chdir(cd)
                os.environ["OMP_NUM_THREADS"] = "{}".format(self.threads)
                sp.check_call(
                    "echo Running with $OMP_NUM_THREADS threads.", shell=True)
                print(self.program)
                print(os.listdir(os.getcwd()))
                sp.check_call("time mpiexec -np {0} ./{1} {2}".format(
                    self.ntasks, self.program, self.optional), shell=True)
                print(self.program)
                os.chdir(cwd + '/..')

            else:
                min_seed = job.pars['MIN_SEED']
                max_seed = job.pars['MAX_SEED']
                qscript = SubmittedScript(self.program, cd=cd,
                                          optional=self.optional,
                                          time=self.time, nodes=self.nodes,
                                          ntasks=self.ntasks,
                                          threads=self.threads,
                                          memory=self.memory,
                                          idx_min=min_seed,
                                          idx_max=max_seed,
                                          queue_name=self.queue_description,
                                          std_out=self.log_path
                                          )
                qscript.send()

            time.sleep(self.delay)

        os.chdir(cwd)

    def set_arguments(self, job, optional_in):
        """
        Set arguments which are used in main program call.
        """

        self.optional = "{} {} {} {}".format(job.module_parameters,
                                             job.system_parameters,
                                             self.out_save_path,
                                             self.queue_description,
                                             optional_in)

    def create_directory(self, job):
        """
        Create the necessary directory structure and copy files
        """

        # If the job's working directory does not yet exist,
        # create it.
        if not os.path.isdir(job.working_directory):
            os.makedirs(job.working_directory)
        # copy the program
        print(self.program)
        print(job.working_directory)

        new_prog = os.path.split(self.program)[1]
        shutil.copy(self.program, job.working_directory + '/' + new_prog)
        self.program = new_prog
        # create the options script
        with open(job.working_directory + '/' + 'slepc.options', 'w') as slepc:
            slepc.write(job.script)

    def set_output_log(self, job):
        """Set path for output log."""
        log_fold = "{}/{}/{}/{}".format(storage, self.description,
                                        self.log_fold,
                                        job.system_parameters)

        # Create the Log subfolder if it does not already exist
        if not os.path.isdir(log_fold):
            os.makedirs(log_fold)

        self.log_path = "{}/{}".format(log_fold, job.module_parameters)


class SubmittedScript(object):
    """
    A script submitted to slurm

    """

    submitted_script = ""

    def __init__(self, name, cd=".", optional='', time="23:59:59",
                 nodes=1, ntasks=1, idx_min=1, idx_max=1, threads=1,
                 memory=4, std_out='Default.out', queue_name='PETSC_DIAG'):
        super(SubmittedScript, self).__init__()

        self.submitted_script = """#!/bin/bash
#SBATCH --array={3}-{4}
#SBATCH --time={0}
#SBATCH --nodes={1}
#SBATCH --ntasks={2}
#SBATCH --output={10}_%A_%a.out
#SBATCH --cpus-per-task={5}
#SBATCH --mem-per-cpu={6}
#SBATCH --job-name {7}
#SBATCH --distribution=cyclic:cyclic
##SBATCH --exclude=compute-1-[1-2,5-16,18-22,24,26-28]


cd {8}
hostname
pwd

OMP_NUM_THREADS=${{SLURM_CPUS_PER_TASK}}

time mpiexec ./{9} {11} -random_seed ${{SLURM_ARRAY_TASK_ID}}

    """.format(time, nodes, ntasks, idx_min, idx_max, threads, memory * 1000,
               queue_name, cd, name, std_out, optional)

# echo "My SLURM_ARRAY_TASK_ID: ${{SLURM_ARRAY_TASK_ID}}"
# echo "Optional command line args: {11}"
    def send(self):
        """
        Send script to batch queue
        """
        if not os.path.isdir('Temp'):
            os.makedirs('Temp')

        with open('Temp/sbatch.run', 'w') as h_sb:
            h_sb.write(self.submitted_script)

        sp.check_call('sbatch Temp/sbatch.run', shell=True)


# Helper functions
def parse_from_dict(key, dict_in):
    """Get value for key in dictionary.

       If the key does not exist, get the default entry."""

    vals = [v for k, v in dict_in.items() if k in key]

    if len(vals) == 0:
        vals = [dict_in["def"]]

    return vals
