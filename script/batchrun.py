"""
This script provides a simple functionality
for running shift-and invert partial
diagonalization types of jobs in parallel
on a cluster running SLURM. Underlying
routines are defined in the batch_misc.py
module.

Parameters:

bm.storage -> string, path to the location,
              preferably on scratch, where
              the executables are copied to
              and then run from.

params -> dict, a dictionary of various job
              parameter values with which the
              jobs are to be run on the cluster.
              The batch_misc.py routines pack
              all possible combinations of
              different parameters into a list
              of job parameters.

redef -> dict, a dictionary one should provide
              if additional parameters are needed
              or if their existing definitions from
              batch_misc.py need to be changed.
              Useful for instance when one would like
              to make sure that the number of up spins
              NU changes together with system size
              s_size, so that the total magnetisation
              is preserved.

description -> string, a human readable description of the
              job. When jobs are run in the cluster,
              executables are copied in the parent directory
              with the following structure:

              path = bm.storage + '/' + description

bs -> BatchSender object. An instance of the BatchSender
              class defined in Batch_misc.py, which contains
              all the required functionality for running jobs
              on the cluster.

bs.out_save_path -> string, root folder for storing the results
                    of the calculations.

bs.delay -> delay (in seconds) when issuing different jobs to
            the cluster.

bs.nodes -> sets the environment variable $SLURM_JOB_NUM_NODES,
            e.g. the number of nodes to be used in the calculation.


bs.ntasks -> sets the SLURM environment variable $SLURM_NTASKS. If
             bs.ntaks > 1, MPI parallelism is used.

bs.threads -> sets the SLURM environment variable
              $SLURM_CPUS_PER_TASK. If bs.ntasks = 1 and bs.threads>1,
              then OpenMP parallelism is used. In the opposite case,
              MPI parallelism is used.

bs.memory -> sets the SLURM environment variable $SLURM_MEM_PER_CPU in
             units of 1000MB.
bs.time -> string, format: "D-HH:MM:SS" (days, hours, minutes, seconds)

bs.run_jobs(no_queue=True/False); the no_queue argument determines
          whether the jobs are sent to the batch queue (if False)
          or are they run directly on the head node instead.

RUNNING THE SCRIPT:

In the folder containing the script, issue the command:

python batchrun prog_name optional

where prog_name is the name of the program to run
(for instance partial_diag) and optional are possible
additional job flags. Currently, none have yet been implemented.
"""

import batch_misc as bm
import numpy as np

bm.storage = "/scratch/jan/sinvert_mbl"


params = {'NU': [6],
          'ne': [0],
          's_size': [12],
          'J1': [-1.],
          'J2': [0.],
          'V1': [1.],  # [1.1],
          'V2': [0],  # [1.1],
          'W': [0], # np.arange(0.5, 9.75, 0.25), 
	  #[5.25, 5.5, 5.75],
          # [5.25, 5.5, 5.75, 6.25, 6.5, 6.75, 7.5, 7.75, 8.25, 8.5, 8.75, 9.25, 9.5],
          # [0.5, 0.75, 1.25, 1.5, 1.75, 5.25, 5.5, 5.75, 6.25, 6.5, 6.75, 7.25, 7.5, 7.75],
          # [2.25, 2.5, 2.75, 3.25, 3.5, 3.75, 4.25, 4.5, 4.75],
          # np.append(np.arange(1., 9., 1.),
          # [2.25, 2.5, 2.75, 3.25, 3.5, 3.75, 4.25, 4.5, 4.75]),
          # 'W' : np.arange(1., 9., 1),
          'base': [0],
          'MIN_SEED': [1],
          'MAX_SEED': [100],
          'nev': [500],
          'dim': [1]
          }

redef = {}

description = "no_doping_XXX_check_ergodicity_snn_spin_dis_new_ordering"

bs = bm.BatchSender(description, redef, **params)

# root folder for saving the results files.
bs.out_save_path = ("/scratch/jan/MBLexact/Results/"
                    "no_doping_XXX_check_ergodicity_snn_spin_dis_new_ordering/"
                    "Data/Eigensystems/"
                    "hcb_hop_nn_exhcb_hop_snn_exhcb_rd_field_ex/")
bs.delay = 1

# number of MPI tasks ->
# if bs.ntaks = 1, no MPI threading is used
bs.ntasks = 1
# number of threads to use
# if bs.threads > 1, then OpenMP threading is used
bs.threads = 20
# memory per CPU unit * 1000 (in MB)
bs.memory = 4
# time requested for the job
bs.time = "02:59:59"
bs.run_jobs(no_queue=True)
